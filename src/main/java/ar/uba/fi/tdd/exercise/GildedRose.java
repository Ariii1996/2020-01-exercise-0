
package ar.uba.fi.tdd.exercise;

class GildedRose {

    public Item[] items;
    private int maxQuality = 50;
    private int minQuality = 0;

    public GildedRose(Item[] _items) {
        items = _items;
    }

    private void mantainItemQualityInRange (Item item){

        if (item.quality < minQuality) item.quality = minQuality;
        if (item.quality > maxQuality) item.quality = maxQuality;
    }

    private void updateBacksagePassesQuality (Item item, int sellIn){

        if (sellIn > 10) item.quality += 1;
                    else if (sellIn > 5) item.quality += 2;
                        else if (sellIn >= 0) item.quality += 3;
                            else item.quality = 0;
    }

    private void addQuality (Item item, int sellIn, int quantityBeforeSellDay, int quantityAfterSellDay){
        
        item.quality += (sellIn >= 0) ? quantityBeforeSellDay : quantityAfterSellDay; 
    }
    
    public void updateItemsQuality() {
        
        for (int item = 0; item < items.length; item++) {

            if (items[item].Name.equals("Sulfuras, Hand of Ragnaros")) continue;

            items[item].sellIn -= 1;

            int sellIn = items[item].sellIn;

            switch(items[item].Name){
                case "General" : 
                    addQuality(items[item], sellIn, -1, -2);
                    break;
                
                case "Backstage passes to a TAFKAL80ETC concert" :
                    updateBacksagePassesQuality(items[item], sellIn);
                    break;

                case "Aged Brie" :
                    addQuality(items[item], sellIn, 1, 2);
                    break;

                case "Conjured" :
                    addQuality(items[item], sellIn, -2, -4);
                    break;    
                }

            mantainItemQualityInRange(items[item]);
        }
    }
}
