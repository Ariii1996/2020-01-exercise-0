package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void AgeBrieIncreasesQuality() {
			
		Item[] items = new Item[] { new Item("Aged Brie", 10, 20) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(21);
	}

	@Test
	public void AgeBrieDontPassQualityAllowed() {

		Item[] items = new Item[] { new Item("Aged Brie", -3, 50) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(50);
	}

	@Test
	public void AgeBrieDecreasesSellin() {
			
		Item[] items = new Item[] { new Item("Aged Brie", 10, 20) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].sellIn).isEqualTo(9);
	}

	@Test
	public void AgeBrieIncreasesQualityAfterSellDay() {
			
		Item[] items = new Item[] { new Item("Aged Brie", -1, 20) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(22);
	}

	@Test
	public void AgeBrieIncreasesAndDontPassQualityAfterSellDay() {
			
		Item[] items = new Item[] { new Item("Aged Brie", -1, 49) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(50);
	}

	@Test
	public void SulfurasMantainsQuality() {

		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 80) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(80);
	}

	@Test
	public void SulfurasMantainsSellin() {

		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 80) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].sellIn).isEqualTo(10);
	}

	@Test
	public void BackstagePassesIncreasesQualityOverTenDays() {

		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 20, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(31);
	}
	
	@Test
	public void BackstagePassesIncreasesQualityUnderTenDays() {

		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 9, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(32);
	}

	@Test
	public void BackstagePassesIncreasesQualityUnderFiveDays() {

		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 4, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(33);
	}

	@Test
	public void BackstagePassesDropsQualityToZero() {

		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(0);
	}

	@Test
	public void BackstagePassesDecreasesSellin() {

		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 15, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].sellIn).isEqualTo(14);
	}

	@Test
	public void BackstagePassesDontPassQualityAllowed() {

		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 3, 49) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(50);
	}

	@Test
	public void GeneralItemDecreasesQuality() {

		Item[] items = new Item[] { new Item("General", 15, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(29);
	}

	@Test
	public void GeneralItemDecreasesQualityAfterSellDay() {

		Item[] items = new Item[] { new Item("General", 0, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(28);
	}

	@Test
	public void GeneralItemDecreasesSellin() {

		Item[] items = new Item[] { new Item("General", 6, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].sellIn).isEqualTo(5);
	}

	@Test
	public void GeneralItemPositiveQuality() {

		Item[] items = new Item[] { new Item("General", 6, 0) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(0);
	}

	@Test
	public void GeneralItemPositiveQualityAfterSellDay() {

		Item[] items = new Item[] { new Item("General", -3, 1) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(0);
	}

	@Test
	public void ConjuredItemDecreasesQuality() {

		Item[] items = new Item[] { new Item("Conjured", 15, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(28);
	}

	@Test
	public void ConjuredItemDecreasesQualityAfterSellDay() {

		Item[] items = new Item[] { new Item("Conjured", 0, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(26);
	}

	@Test
	public void ConjuredItemDecreasesSellin() {

		Item[] items = new Item[] { new Item("Conjured", 6, 30) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].sellIn).isEqualTo(5);
	}

	@Test
	public void ConjuredItemPositiveQuality() {

		Item[] items = new Item[] { new Item("Conjured", 6, 0) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(0);
	}

	@Test
	public void ConjuredItemPositiveQualityAfterSellDay() {

		Item[] items = new Item[] { new Item("Conjured", -3, 2) };
		GildedRose app = new GildedRose(items);

		app.updateItemsQuality();

		assertThat(items[0].quality).isEqualTo(0);
	}
}
